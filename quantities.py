import sys

items = {}

def op_add():

    try:
        item = str(sys.argv[0])
        quantity = int(sys.argv[1])
    except:
        ValueError("La cantidad debe ser un número entero.")

    new_item = True

    items[item] = quantity
    for key, value in items.items():
        if key == item:
            new_item = False
    if not new_item:
        items.pop(key)
        items[item] = quantity


def op_items():

    items_list = []
    for key, value in items.items():
        items_list.append(key)
    item_str = " ".join(items_list)
    print("Items: " + item_str)

def op_all():

    all_list = []
    for key, value in items.items():
        all_list.append(key + " " + "(" + str(value) + ")")
    all_str = " ".join(all_list)
    print("All: " + all_str)

def op_sum():

    suma = 0
    for key, value in items.items():
        suma = suma + value
    print("Sum: " + str(suma))


def main():

    while sys.argv:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "all":
            op_all()
        elif op == "sum":
            op_sum()


if __name__ == '__main__':
    main()
